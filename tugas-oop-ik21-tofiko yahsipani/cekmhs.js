var xmlHttp = createXmlHttpRequestObject();

		//membuat obyek XMLHttpRequset
		function createXmlHttpRequestObject()
		{
    		var xmlHttp;

    		//utk browser IE
    		if(window.ActiveXObject)
    		{
        		try
        		{
            		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        		}
        		catch (e)
        		{
            		xmlHttp = false;
        		}
    		}

    		//untuk browser lain
    		else
    		{
        		try
        		{
            		xmlHttp = new XMLHttpRequest();
        		}
        		catch (e)
        		{
            		xmlHttp = false;
        		}
    		}

    		//muncul pesan apabila object xmlhttprequest gagal dibuat
    		if(!xmlHttp) alert("Obyek XMLHttpRequest gagal dibuat")
    		else
    			return xmlHttp;
		}
		function process()
		{
			//akan di proses hanya bila obyek XMLHttpRequest tidak sibuk
			if (xmlHttp.readyState == 4 || xmlHttp.readyState == 0)
			{
				nim = encodeURIComponent(document.getElementById("nim").value);	
			
			//merequest file cek.php di server secara asyncrounous
			xmlHttp.open("GET","get2.php?nim=" + nim, true);
			
			//mendefinisikan metode yang dilakukan apabila memperoleh respon server
			xmlHttp.onreadystatechange = handleServerResponse;
			
			//membuat request ke server
			xmlHttp.send(null);
			}
			
			else
			{
			//jika server sibuk,request akan dilakukan lagi setelah satu detik
			setTimeout('process()',1000)	
			}
		}
		function handleServerResponse()
		{
			//jika proses request telah selesai dan menerima respon
			if(xmlHttp.readyState == 4)
			{
				//jika request ke server sukses
				if(xmlHttp.status == 200)
				{
					//mengambil dokumen xml yang diterima dari server
					xmlResponse = xmlHttp.responseXML;
					
					//memperoleh elemen dokumen(root elemen) dari xml
					xmlDocumentElement = xmlResponse.documentElement;
					
					//membaca data elemen
					hasil = xmlDocumentElement.firstChild.data;
					
					//akan mengupdate tampilan halaman web pada elemen bernama respon
					document.getElementById("respon").innerHTML = '<i>' + hasil + '</i>';
					
					//request akan dilakukan lagi setelah satu detik(automatic request)
					setTimeout('process()',1000);
				}
				else
				{
					//akan muncul pesan apabila terjadi masalh dalam mengakses server (selain respon 200)
					alert("Terjadi masalh dalam mengakses server" + xmlHttp.statusText);	
				}
			}
		}
		