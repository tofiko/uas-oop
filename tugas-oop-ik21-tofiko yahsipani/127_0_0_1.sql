-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2019 at 11:30 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajarajax`
--
CREATE DATABASE IF NOT EXISTS `belajarajax` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `belajarajax`;

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` varchar(20) NOT NULL,
  `namalengkap` varchar(150) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `tempatlahir` varchar(100) NOT NULL,
  `tgllahir` date NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `namalengkap`, `kelas`, `tempatlahir`, `tgllahir`, `email`) VALUES
('170442130001', 'Bahar', 'ik', 'Jakarta', '1998-01-01', 'bahar@gmail.com'),
('170442130002', 'adib', 'ap', 'Jakarta', '1998-02-02', 'adib@gmail.com'),
('170442130003', 'Alfath', 'abi', 'Jakarta', '1998-03-03', 'alfath@gmail.com'),
('170442130004', 'Diyas', 'ka', 'Jakarta', '1998-04-04', 'diyas@gmail.com'),
('170442130005', 'Doris', 'ap', 'Jakarta', '2019-05-05', 'doris@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
