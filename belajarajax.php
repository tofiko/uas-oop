<!DOCTYPE>
<html>
	<head>
		<title>Belajar AJAX</title>
	<script type="text/javascript">
		var xmlHttp = createXmlHttpRequestobject();

		//membuat obyek XMLHttpRequset
		function createXmlHttpRequestobject()
		{
    		var xmlHttp;

    		//utk browser IE
    		if(window.ActiveXObject)
    		{
        		try
        		{
            		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        		}
        		catch (e)
        		{
            		xmlHttp = false;
        		}
    		}

    		//untuk browser lain
    		else
    		{
        		try
        		{
            		xmlHttp = new XMLHttpRequest();
        		}
        		catch (e)
        		{
            		xmlHttp = false;
        		}
    		}

    		//muncul pesan apabila object xmlhttprequest gagal dibuat
    		if(!xmlHttp) alert("Obyek XMLHttpRequest gagal dibuat")
    		else
    			return xmlHttp;
		}
	</script>
	</head>

	<body>
		<form>
			<p>Input Jenis Makanan</p>
			<input type="text" name="makanan"><br>
			Nama Yang Anda Input Adalah
		</form>
	</body>
</html>